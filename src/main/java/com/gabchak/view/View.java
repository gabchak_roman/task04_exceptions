/***
 * view.
 */
package com.gabchak.view;

/**
 * This is FunctionalInterface.
 */
@FunctionalInterface
public interface View {

    /**
     * Method print to implement.
     */
    void print();
}
