/***
 * view.
 */
package com.gabchak.view;

import com.gabchak.controller.Controller;
import com.gabchak.controller.ControllerImpl;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/***
 * Class View.
 */
public class ViewImpl {

    /***
     * Takes the input from the keyboard.
     */
    private static Scanner input = new Scanner(System.in);

    /***
     * Instance of Controller.
     */
    private Controller controller;

    /***
     *hashmap saves the keys and values.
     */
    private Map<String, String> menu;

    /***
     * hashmap saves the keys and action.
     */
    private Map<String, View> methodsMenu;

    /***
     * The method takes from the user input text and key.
     * @param text - the input text.
     * @param key - the key text.
     */
    public ViewImpl(final String text, final String key) {

        this.controller = new ControllerImpl();
        this.controller.setText(text);
        this.controller.setKey(key);
        menu = new LinkedHashMap<>();

        menu.put("1", "|           1 - Show text.               |");
        menu.put("2", "|           2 - Encrypt the text.        |");
        menu.put("3", "|           3 - Decrypt the text.        |");
        menu.put("Q", "|           Q - exit                     |");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
    }

    /***
     * When the input is 1.
     * Method prints text on the screen.
     */
    private void pressButton1() {
        System.out.println(controller.getText());
    }

    /***
     * When the input is 2.
     * The method starts encrypt text.
     */
    private void pressButton2() {
        controller.encrypt();
    }

    /***
     * When the input is 3.
     * The method starts decrypt text.
     */
    private void pressButton3() {
        controller.decrypt();
    }

    //-------------------------------------------------------------------------


    /***
     * Method output Menu.
     */
    private void outputMenu() {
        System.out.println("►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄");
        System.out.println("                  MENU:");
        System.out.println("►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄►◄");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    /**
     * Method show menu and get input from the user.
     */
     public final void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                System.out.println();
            }
        } while (!keyMenu.equals("Q"));
    }
}