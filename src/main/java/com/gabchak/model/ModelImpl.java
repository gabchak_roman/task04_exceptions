package com.gabchak.model;

import com.gabchak.exceptions.EncryptionStatusException;
import com.gabchak.exceptions.WrongKeyInputException;
import com.gabchak.exceptions.WrongTextInputException;

import java.util.ArrayList;
import java.util.List;

/***
 * The class ModelImpl implements Model.
 */
public class ModelImpl implements Model {

    /***
     * The numeric range of lowercase English letters from the Ascii Table.
     */
    private static final int[] ALPHABET_LOWER_CASE_START_END = {97, 122};
    /***
     * The numeric range of Uppercase English letters from the Ascii Table.
     */
    private static final int[] ALPHABET_UPPER_CASE_START_END = {65, 90};

    /***
     * The input text.
     */
    private String text;

    /***
     * The input key.
     */
    private String key;

    /***
     * Text patter status.
     */
    private boolean textStatus = true;

    /***
     * Key patter status.
     */
    private boolean keyStatus = true;

    /***
     * The encryption status.
     */
    private boolean encryptionStatus = false;

    /***
     * The method returns text.
     * @return - text;
     */
    public final String getText() {
        return text;
    }

    /***
     * The method Initializes a variable text.
     * @param text - get input text.
     */
    public final void setText(final String text) throws WrongTextInputException {
        this.text = text;
        checkingInputText();
        if (!textStatus) {
            throw new WrongTextInputException();
        }
    }

    /***
     * The method Initializes a variable key.
     * @param key - get input key.
     */
    public final void setKey(final String key) throws WrongKeyInputException {
        this.key = key;
        checkingInputKey();
        if (!keyStatus) {
            throw new WrongKeyInputException("KEY: "
                    + "The key must contain at least one"
                    + " lower case letter\n"
                    + "one upper case letter and digit.\n"
                    + "KEY: The key length can not be less "
                    + "than 6 characters and not more than 16.");
        }
    }

    /***
     * A method that encrypts text with a given key.
     */
    public final void encrypt() throws EncryptionStatusException {
        if (encryptionStatus) {
            throw new EncryptionStatusException(
                    "The text is already encrypted.");

        }
        char[] textChars = text.toCharArray();
        int[] keysLocal = keys(key);
        int counter = 0;
        for (int i = 0; i < textChars.length; i++) {
            rollRight(textChars, i, keysLocal[counter++]);
            if (counter == key.length()) {
                counter = 0;
            }
        }
        text = String.valueOf(textChars);
        encryptionStatus = true;
    }

    /***
     * A method that decrypts text by a given key.
     */
    public final void decrypt() throws EncryptionStatusException {
        if (!encryptionStatus) {
            throw new EncryptionStatusException(
                    "The text has not been encrypted yet.");
        }
        char[] textChars = text.toCharArray();
        int[] keysLocal = keys(key);
        int counter = 0;
        for (int i = 0; i < textChars.length; i++) {
            rollLeft(textChars, i, keysLocal[counter++]);
            if (counter == key.length()) {
                counter = 0;
            }
        }
        text = String.valueOf(textChars);
        encryptionStatus = false;
    }

    /***
     * The method accepts an array of characters and shifts the number.
     * of a particular character by the number 'n' to the right;
     * @param arr - accepts an array of chars.
     * @param index - the index of the particular character to be move.
     * @param n - indicates how many units the character
     *          symbol must be replaced move.
     */
    private void rollRight(final char[] arr, final int index, final int n) {
        int upperStart = ALPHABET_UPPER_CASE_START_END[0];
        int upperEnd = ALPHABET_UPPER_CASE_START_END[1];
        int lowerStart = ALPHABET_LOWER_CASE_START_END[0];
        int lowerEnd = ALPHABET_LOWER_CASE_START_END[1];
        if (arr[index] >= lowerStart && arr[index] <= lowerEnd) {
            if ((arr[index] + n) > lowerEnd) {
                arr[index] = (char)
                        (lowerStart + ((arr[index] + (n)) % lowerEnd));
            } else {
                arr[index] = (char) (arr[index] + n);
            }
        } else if (arr[index] >= upperStart && arr[index] <= upperEnd) {
            if ((arr[index] + n) > upperEnd) {
                arr[index] = (char)
                        (upperStart + ((arr[index] + (n)) % upperEnd));
            } else {
                arr[index] = (char) (arr[index] + n);
            }
        }
    }

    /***
     * The method accepts an array of characters and shifts the number.
     * of a particular character by the number 'n' to the left;
     * @param arr - accepts an array of chars.
     * @param index - the index of the particular character to be move.
     * @param n - indicates how many units the
     *          character symbol must be replaced move.
     */
    private void rollLeft(final char[] arr, final int index, final int n) {
        int upperStart = ALPHABET_UPPER_CASE_START_END[0];
        int upperEnd = ALPHABET_UPPER_CASE_START_END[1];
        int lowerStart = ALPHABET_LOWER_CASE_START_END[0];
        int lowerEnd = ALPHABET_LOWER_CASE_START_END[1];
        if (arr[index] >= lowerStart && arr[index] <= lowerEnd) {
            if ((arr[index] - n) < lowerStart) {
                arr[index] = (char)
                        (lowerEnd - (lowerStart % (arr[index] - n)));
            } else {
                arr[index] = (char) (arr[index] - n);
            }
        } else if (arr[index] >= upperStart && arr[index] <= upperEnd) {
            if ((arr[index] - n) < upperStart) {
                arr[index] = (char)
                        (upperEnd - (upperStart % (arr[index] - n)));
            } else {
                arr[index] = (char) (arr[index] - n);
            }
        }
    }

    /***
     * The method checks the input text for certain parameters.
     */
    private void checkingInputText() {
        if (text == null) {
            textStatus = false;
            return;
        }
        String textPatter = "[ \nA-Za-z]*";
        if (!text.matches(textPatter)) {
            textStatus = false;
        }
    }

    /***
     * The method checks the key text for certain parameters.
     */
    private void checkingInputKey() {
        if (key == null) {
            keyStatus = false;
            return;
        }
        String pattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,16}$";
        final int minLength = 4;
        final int maxLength = 16;
        int keyLength = key.length();
        if (keyLength < minLength || keyLength > maxLength) {
            keyStatus = false;
            return;
        }
        if (!key.matches(pattern)) {
            keyStatus = false;
        }
    }

    /***
     * The method takes a string and converts it to an array of numbers.
     * @param key -accepts the text key.
     * @return - returns an array of numbers.
     */
    private int[] keys(final String key) {
        final int dev = 10;
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < key.length(); i++) {
            int temp = key.charAt(i);
            while (temp != 0) {
                if (temp % dev == 0) {
                    list.add(dev);
                } else {
                    list.add(temp % dev);
                }
                temp = temp / dev;
            }
        }
        return list.stream().mapToInt(i -> i).toArray();
    }

}
