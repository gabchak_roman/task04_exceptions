/***
 * model.
 */
package com.gabchak.model;

import com.gabchak.exceptions.EncryptionStatusException;
import com.gabchak.exceptions.WrongKeyInputException;
import com.gabchak.exceptions.WrongTextInputException;

/***
 * Model interface.
 */
public interface Model {

    /***
     * The method Initializes a variable text.
     * @param text - get input text.
     */
    void setText(String text) throws WrongTextInputException;

    /***
     * The method returns text.
     * @return - text.
     */
    String getText();

    /***
     * The method Initializes a variable key.
     * @param key - get input key.
     */
    void setKey(String key) throws WrongKeyInputException;

    /***
     * A method that encrypts text with a given key.
     */
    void encrypt() throws EncryptionStatusException;

    /***
     * A method that decrypts text by a given key.
     */
    void decrypt() throws EncryptionStatusException;
}
