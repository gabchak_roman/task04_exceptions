package com.gabchak.controller;

import com.gabchak.exceptions.EncryptionStatusException;
import com.gabchak.exceptions.WrongKeyInputException;
import com.gabchak.exceptions.WrongTextInputException;
import com.gabchak.model.Model;
import com.gabchak.model.ModelImpl;

/***
 * The class ControllerImpl implements Controller.
 */
public class ControllerImpl implements Controller {

    /***
     * The model instance.
     */
    private Model model;

    /***
     * Constructor ControllerImpl which initialize model.
     */
    public ControllerImpl() {
        this.model = new ModelImpl();
    }

    /***
     * The method returns text.
     * @return - text.
     */
    @Override
    public final String getText() {
        return model.getText();
    }

    /***
     * The method Initializes a variable text.
     * @param text - get input text.
     */
    @Override
    public final void setText(final String text) {
        try {
            model.setText(text);
        } catch (WrongTextInputException e) {
            System.out.println("-".repeat(42) + "\n"
                    + e.getMessage() + "\n" + "-".repeat(42));
        }
    }

    /***
     * The method Initializes a variable key.
     * @param key - get input key.
     */
    @Override
    public final void setKey(final String key) {
        try {
            model.setKey(key);
        } catch (WrongKeyInputException e) {
            System.out.println("-".repeat(42) + "\n"
                    + e.getMessage() + "\n" + "-".repeat(42));
        }
    }

    /***
     * A method that encrypts text with a given key.
     */
    @Override
    public final void encrypt() {
        try {
            model.encrypt();
        } catch (EncryptionStatusException e) {
            System.out.println("-".repeat(42) + "\n"
                    + e.getMessage() + "\n" + "-".repeat(42));
        }
    }

    /***
     * A method that decrypts text by a given key.
     */
    @Override
    public final void decrypt() {
        try {
            model.decrypt();
        } catch (EncryptionStatusException e) {
            System.out.println("-".repeat(42) + "\n"
                    + e.getMessage() + "\n" + "-".repeat(42));
        }
    }
}
