/***
 * controller.
 */
package com.gabchak.controller;

/***
 * The Controller interface.
 */
public interface Controller {

    /***
     * The method returns text.
     * @return - text.
     */
    String getText();

    /***
     * The method Initializes a variable text.
     * @param text - get input text.
     */
    void setText(String text);

    /***
     * The method Initializes a variable key.
     * @param key - get input key.
     */
    void setKey(String key);

    /***
     * A method that encrypts text with a given key.
     */
    void encrypt();

    /***
     * A method that decrypts text by a given key.
     */
    void decrypt();
}
