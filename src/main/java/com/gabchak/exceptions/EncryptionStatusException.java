package com.gabchak.exceptions;

/***
 *  The class EncryptionStatusException extends Exception.
 *  An exception may occur if you are trying to encrypt
 *  text that has already been encrypted.
 */
public class EncryptionStatusException extends Exception {

    /***
     * Constructor of EncryptionStatusException without parameters.
     */
    public EncryptionStatusException() {
        super();
    }

    /***
     * Constructor of EncryptionStatusException which takes a string.
     * @param message - get description of exception.
     */
    public EncryptionStatusException(final String message) {
        super(message);
    }
}
