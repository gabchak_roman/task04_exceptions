package com.gabchak.exceptions;

/***
 *  The class WrongKeyInputException extends Exception.
 *  An exception may occur if the key does not matched the pattern.
 */
public class WrongTextInputException extends Exception {

    /***
     * Constructor of WrongKeyInputException without parameters.
     */
    public WrongTextInputException() {
    }

    /***
     * Constructor of WrongTextInputException which takes a string.
     * @param message - get description of exception.
     */
    public WrongTextInputException(final String message) {
        super(message);
    }
}
