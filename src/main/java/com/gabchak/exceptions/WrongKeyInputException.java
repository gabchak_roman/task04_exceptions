package com.gabchak.exceptions;

/***
 *  The class WrongKeyInputException extends Exception.
 *  An exception may occur if the text does not matched the pattern.
 */
public class WrongKeyInputException extends Exception {

    /***
     * Constructor of WrongKeyInputException without parameters.
     */
    public WrongKeyInputException() {
    }

    /***
     * Constructor of WrongKeyInputException which takes a string.
     * @param message - get description of exception.
     */
    public WrongKeyInputException(final String message) {
        super(message);
    }
}
