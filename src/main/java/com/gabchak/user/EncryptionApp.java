/***
 * user.
 */
package com.gabchak.user;

import com.gabchak.view.ViewImpl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/***
 * The class EncryptionApp.
 */
public final class EncryptionApp {
    /***
     * Constructor of EncryptionApp without parameters.
     */
    private EncryptionApp() {
    }

    /***
     * Starting point that launches the program.
     * @param args - starting point parameters.
     */
    public static void main(final String[] args) {

        ViewImpl view = new ViewImpl(text(), "Roman123");
        view.show();

    }

    /***
     * A method reading text from a file and return the string.
     * @return - return the text.
     */
    private static String text() {
        File file = new File("text");
        Scanner scanner = null;
        try {
            scanner = new Scanner(new FileReader(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        StringBuilder stringBuffer = new StringBuilder();
        assert scanner != null;
        while (scanner.hasNextLine()) {
            stringBuffer.append(scanner.nextLine()).append("\n");
        }
        scanner.close();
        return String.valueOf(stringBuffer);
    }
}
